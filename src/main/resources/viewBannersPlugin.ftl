[#-- @ftlvariable name="action" type="com.pronetbeans.bamboo.banners.BannersGlobalConfigurePlugin" --]
[#-- @ftlvariable name="" type="com.pronetbeans.bamboo.banners.BannersGlobalConfigurePlugin" --]

<html>
<head>
    [@ui.header pageKey="Bamboo System Banner Configuration" title=true /]
    <meta name="decorator" content="adminpage">
</head>
<body>
<style type="text/css">
</style>

 [@ww.form id='configureBannersPlugin' action='updateConfigureBannersPlugin.action' submitLabelKey='Save Configuration' titleKey='System Banner']

<br><br>

[@ui.bambooSection]
   [@ww.textarea required='false' name='bannerSystem' id='bannerSystem'/]<br>
   To disable the System Banner, delete all the content in the textarea.
[/@ui.bambooSection]

        [@ui.clear /]

    [/@ww.form]

        <br><br>
[@ui.bambooSection title='Suggested Messages & Formats']
<br>
&lt;br&gt;&lt;center&gt;&lt;span style="color: white;"&gt;The system will be down for maintenance on MONTH, DAY, YEAR&lt;/span&gt;&lt;/center&gt;
<br><br><br>
&lt;br&gt;&lt;center&gt;&lt;a style="color: red;" target="_blank" href="http://www.atlassian.com/software/bamboo/whats-new"&gt;What's New In Our Bamboo System&lt;/a&gt;&lt;/center&gt;
[@ui.clear /]
[/@ui.bambooSection]
<br><br>

</body>
</html>