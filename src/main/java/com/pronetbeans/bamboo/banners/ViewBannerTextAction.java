package com.pronetbeans.bamboo.banners;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.atlassian.bandana.BandanaManager;

/**
 *
 * @author Adam Myatt
 */
public class ViewBannerTextAction extends BambooActionSupport {

    @SuppressWarnings("UnusedDeclaration")
    private BandanaManager bandanaManager;
    private String bannerSystem;

    /**
     * @return the bannerSystem
     */
    public String getBannerSystem() {
        return bannerSystem;
    }

    /**
     * @param bannerSystem the bannerSystem to set
     */
    public void setBannerSystem(String bannerSystem) {
        this.bannerSystem = bannerSystem;
    }

    @Override
    public String doInput() throws Exception {

        // load value(s) from Bandana storage into field for display in UI
        this.bannerSystem = getValueAsString(Constants.BANNER_SYSTEM_KEY);

        return "view";
    }

    public void setBandanaManager(BandanaManager bandanaManager) {
        this.bandanaManager = bandanaManager;
    }

    public String getValueAsString(String key) {
        Object o = bandanaManager.getValue(PlanAwareBandanaContext.GLOBAL_CONTEXT, key);
        if (o == null) {
            return "";
        } else {
            return (String) o;
        }
    }
}
